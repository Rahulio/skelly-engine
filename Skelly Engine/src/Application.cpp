#include "Game.h"

#include <iostream>

int main(void)
{
  Game* game = new Game("Skelly Engine", 1280, 720);
  if(game->Initialise() == -1) return -1;
  
  bool gameRunning = true;
  /* Loop until the user closes the window */
  while (game->IsRunning())
  {
    game->Update();
    game->Render();
    game->HandleEvents();
  }
}