#include "Game.h"
#include "Window.h"

Game::Game(const char* title, unsigned int width, unsigned int height) {
  window = new Window(title, width, height);
  isRunning = false;
}

Game::~Game() {
  delete(window);
}

int Game::Initialise() {
  if (window->Initialise() == -1)
    return -1;

  isRunning = true;
  return 0;
  // std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << '\n';
}

void Game::Update() {
  // TODO: Add update logic here
}

void Game::Render() {
  glClearColor(0.2f, 0.9f, 0.85f, 1.0f);  // A nice sky blue colour :)
  glClear(GL_COLOR_BUFFER_BIT);

  glBegin(GL_TRIANGLES);
  glColor3f(1.0f, 0.0f, 0.0f);
  glVertex2f(-0.5f, -0.5f);
  glColor3f(0.0f, 1.0f, 0.0f);
  glVertex2f(0.0f, 0.5f);
  glColor3f(0.0f, 0.0f, 1.0f);
  glVertex2f(0.5f, -0.5f);
  glEnd();

  glfwSwapBuffers(window->GetWindow());
}

void Game::HandleEvents() {
  glfwPollEvents();

  if (glfwWindowShouldClose(window->GetWindow())) isRunning = false;
}