#include "Window.h"

GLFWwindow* Window::window = nullptr;

Window::Window(const char* title, unsigned int width, unsigned int height) {
  window = nullptr;

  this->title = title;
  this->width = width;
  this->height = height;
}

Window::~Window() {
  glfwTerminate();
}

int Window::Initialise() {
  if (!glfwInit())
    return -1;

  window = glfwCreateWindow(width, height, title, NULL, NULL);
  if (!window)
    return -1;

  glfwMakeContextCurrent(window);

  if (glewInit() != GLEW_OK)
    return -1;

  return 0;
}