#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Window {
public:
  Window(const char* title, unsigned int width, unsigned int height);
  ~Window();
  int Initialise();

  static GLFWwindow* GetWindow() { return window; }

private:
  static GLFWwindow* window;
  const char* title;
  unsigned int width;
  unsigned int height;
};