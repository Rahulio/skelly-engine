#pragma once

class Window;

class Game {
public:
  Game(const char* title, unsigned int width, unsigned int height);
  ~Game();

  int Initialise();
  void Update();
  void Render();
  void HandleEvents();

  bool IsRunning() { return isRunning; }

private:
  bool isRunning;

  Window* window;
};